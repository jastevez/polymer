#Imagen base
FROM node:latest
#Directorio de la app
WORKDIR /app
#Copiar archivos
ADD ./build/default /app/build/default
ADD package.json /app
ADD server.js /app
#Dependencias
RUN npm install
# puertos
EXPOSE 3000
# comando
CMD ["npm", "start"]